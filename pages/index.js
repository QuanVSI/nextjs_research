import Head from 'next/head'
import Link from 'next/link'
import AddBlogs from '../components/addBlog';
import Blogs from '../components/blogs'
import styles from '../styles/Home.module.css'
import 'bootstrap/dist/css/bootstrap.min.css'

export async function getServerSideProps() {
  const data_blogs = await fetch(`http://localhost:1337/api/blogs?populate=%2A`);
  const result_blogs = await data_blogs.json()
  const blogs = result_blogs.data;

  const data_topics = await fetch(`http://localhost:1337/api/topics`);
  const result_topics = await data_topics.json()
  const topics = result_topics.data
  let isLoading = true;
  return {
    props: {
      blogs,
      topics,
      isLoading
    }, // will be passed to the page component as props
  }
}

export const Dropdown = (props) => (
  <div className="form-group">
    <strong>{props.name}</strong>
    <select
      className="form-control"
      name="{props.name}"
      onChange={props.onChange}
    >
      <option defaultValue>Select {props.name}</option>
      {props.options.map((item, index) => (
        <option key={index} value={item.id}>
          {item.name}
        </option>
      ))}
    </select>
  </div>
)

export default function Home({ blogs, topics, isLoading }) {
  let listTopic = [];
  let i = 1;
  topics.forEach(element => {
    listTopic.push({
      name: element.attributes.name,
      id: i
    })
    i++;
  });
  const state = {
    name: 'Topic',
    list: listTopic
  }
  return (
    <div className={styles.container}>
      <Head>
        <title >Blog</title>
      </Head>
      <h2 >Blogs</h2>
      
      <hr/>
      <Dropdown
        name={state.name}
        options={state.list}
        />
      {/* { topics.map(topic =>
         <Link href={`/topics/${topic.id}`}>
          <div className={styles.topic}>
            {topic.attributes.name}
          </div>
        </Link>
      )} */}
      
      <Blogs blogs={blogs} />
      <hr/>
      {/* <AddBlogs /> */}

      <script src="https://unpkg.com/react/umd/react.production.min.js" crossorigin></script>

      <script
        src="https://unpkg.com/react-dom/umd/react-dom.production.min.js"
        crossorigin></script>

      <script
        src="https://unpkg.com/react-bootstrap@next/dist/react-bootstrap.min.js"
        crossorigin></script>
     
    </div>
  )
}
