import Head from 'next/head'
import Blogs from '../../../components/blogs'
import styles from '../../../styles/Home.module.css'

export async function getServerSideProps(context) {
  const data_blogs = await fetch(`http://localhost:1337/api/blogs?filters[topic][id][$eq]=${context.params.id}`);
  const result_blogs = await data_blogs.json()
  const blogs = result_blogs.data;

  return {
    props: {
      blogs
    }, // will be passed to the page component as props
  }
}
const Topics = ({ blogs}) => {
  console.log(blogs);
  
  return (
    <div className={styles.container}>
      <Head>
        <title >Blog</title>
      </Head>
      <div className={styles.profile}>
        Quan
      </div>
      <h2 >Blogs</h2>
      <hr/>
      <Blogs blogs={blogs} />
    </div>
  )
}
export default Topics;
