import Image from "next/image"
import food from "../public/img/food.png"
import blogStyle from '../styles/blog.module.css'
const Blog = ({ blog }) => {
    console.log('http://localhost:1337'+blog.attributes.avatar.data[0].attributes.url)
    return (
        <>
            <div className={blogStyle.card}>
                <div className={blogStyle.cardInfo}>
                    <img className={blogStyle.avatar} src={'http://localhost:1337'+ blog.attributes.avatar.data[0].attributes.url} ></img>
                    <div className={blogStyle.title}>
                        <a href="/">{blog.attributes.title}</a>
                    </div>
                    <div className={blogStyle.cardContent}>
                        {blog.attributes.content}
                    </div>
                </div>
            </div>
        </>
    )
}
export default Blog