import styleHeader from '../styles/header.module.css'
import { Search } from '@material-ui/icons';
import Profile from './profile';

const Header = () =>{
    return (
        <>
            <div className={styleHeader.logo}></div>
            <nav className={styleHeader.topnav}>
                <a className={styleHeader.active} href='/'>Home</a>
                <a href='/'>About</a>
                <Profile isLogged={false} />
                <div class={styleHeader.searchContainer}>
                    <form >
                        <input type="text" placeholder="Search.." name="search"/>
                        <button type="submit"><Search className={styleHeader.icon}/></button>
                    </form>
                </div>
            </nav>
        </>
        
    )
}

export default Header;