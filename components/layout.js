
import Header from './header'
import styles from '../styles/Home.module.css'
import Footer from './footer'
const Layout = ({ children }) =>{
    return (
        <>
        <Header/>
        <div className={styles.container}>
            <main className={styles.main}>
                {children}
            </main>
        </div>
        <Footer/>
        </>
    )
}
export default Layout