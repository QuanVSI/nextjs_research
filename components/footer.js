import styleFooter from '../styles/footer.module.css'
import { Mail } from '@material-ui/icons';
import { Facebook } from '@material-ui/icons';
import { Twitter } from '@material-ui/icons';
import { GitHub } from '@material-ui/icons';

const Footer = ()=>{
    return (
        <div className={styleFooter.footer}>
                <div>
                    <a href='/'>Blog.com</a>
                </div>
                <div className={styleFooter.mail}>
                    <Mail className={styleFooter.icon}/>
                    <p className={styleFooter.text}>support@blog.com</p>
                </div>
                <div className={styleFooter.socialNetwork}>
                    <Facebook className={styleFooter.fb}>
                        <a href='/'></a>
                    </Facebook>
                    <GitHub >
                        <a href='/'></a>
                    </GitHub>
                    <Twitter className={styleFooter.twitter} >
                    <a href='/'></a>
                    </Twitter>
                </div>
        </div>
    )
}
export default Footer;