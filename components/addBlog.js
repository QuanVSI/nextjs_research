
import addStyle from '../styles/addBlog.module.css'

const AddBlogs =()=>{
    return (
        <>
        <div className={addStyle.title}>
            Thêm Blog
        </div>
        <form className={addStyle.form}>
            <label className={addStyle.label} for="title">Title:</label>
            <input type="text" name="title" id="title" />
            <br/>
            <label className={addStyle.label} for="content">Content:</label>
            <input type="text" name="content" id="content" />
            <br/>
            <button className={addStyle.submit} type="submit">Add</button>
        </form>
        </>
    )
}

export default AddBlogs;