
function User() {
    return <h1>Welcome</h1>
}
function Guest() {
    return <></>
}

const Profile = (props) =>{
    const isLogged = props.isLogged
    if(isLogged) {
        return <User/>
    }else {
        return <Guest />
    }
}
export default Profile