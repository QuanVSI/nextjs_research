import Blog from "./blog";
const Blogs = ({blogs}) =>{
    return (
       <>
        { blogs.map(blog => <Blog blog={blog}/>) }
       </>
    )
}
export default Blogs